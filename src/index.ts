import {
  BooleanField,
  DateField,
  NumberField,
  OptionField,
  Sheet,
  SpaceConfig,
  TextField,
  Workbook,
  LinkedField,
  ReferenceField
} from '@flatfile/configure'

import { vbc } from './sheets/vbc'
import { qm } from './sheets/qm'
import { claims } from './sheets/claims'
// import { ExcelExtractor } from '@flatfile/plugin-xlsx-extractor';
// import { EventTopic } from '@flatfile/api';
// import { useConfigure } from '@flatfile/listener';

// Need to append the read-only option to each of the sheets for the tenant-creation workbook
// const vbc_readonly = new Sheet('VBC: Read only',vbc,{readOnly:true})
// const qm_readonly = new Sheet('QM: Read only',qm,{readOnly:true})
// const claims_readonly = new Sheet('Claims: Read only',claims,{readOnly:true})


const ClaimsMetadata = new SpaceConfig({
  name: 'Claims Metadata and Data (Eric w/ Excel Parsing)',
  slug: 'claims-metadata-and-data-eric-with-excel',
  workbookConfigs: {
    //Config: a primary action that pushes all VALID data to tenant-creation workbook in the same structure
    //Config: a custom action on each sheet in the collection workbook that pulls the value from the record in the tenant-creation workbook (based on PK) 
    //  and applied a warning if that value is present and different that what is in the data-collection workbook
    //Config: automation to run the comparison check whenever data is changed in the data-collection workbook
    'data-collection':new Workbook({
      name: 'Metadata collection',
      sheets: {
        vbc,
        qm,
        claims
      },
    }),
    //Config: a primary action that does a 3rd party API call to either create or update a tenant
    //Config: if no "tenantId" exists in Space metadata, create a tenant and then return the tenantId as a response and save to Space metadata
    //Config: if "tenantId" exists in Space metadata, update the tenant
    //Config: serialize all data into a JSON array
    //Innovacer Build: endpoint that allows for creation or update of a tenant, depending on whether tenantId exists
    //Config: all sheets are read only, no other differences
    //Config: no guest user access to this workbook (we don't want them clicking to create and update tenants)
      'tenant-creation':new Workbook({
      name: 'Final draft',
      sheets: {
        // vbc_readonly,
        // qm_readonly,
        // claims_readonly
      },
  })
},
});

// Config: Need to have a document with instructions, ideally with a downloadable link to the metadata template, which is the same for every Space with
//  this SpaceConfig
// 
// ClaimsMetadata.on([EventTopic.Spaceadded], (event) => {
//  Add custom pages (uses Flatfile API package)
// });

// Bug: When uploading Excel file using the existing Excel Extractor, we never get to upload completed event
// Innovaccer Build: Need customization to Excel extractor <-- customization to Excel extractor (Flatfile pair with Innovaccer?)
//
// ClaimsMetadata.on([EventTopic.Uploadcompleted], (event) => {
//   return new ExcelExtractor(event, { rawNumbers: true }).runExtraction();
// });

// Config: Need to notify users in the Space if there are outstanding errors in the Collection Workbook
// Innovaccer Build: Cron job that checks for most recent update and then uses Innovaccers notification system to notify the appropriate users
//
// ClaimsMetadata.on([EventTopic.Recordscreated], (event) => {
//  If (timeperiod) since records last updated AND If validation errors <-- cron job created by Innovaccer, use whatever Innovaccer is using for email
// });

export default ClaimsMetadata
