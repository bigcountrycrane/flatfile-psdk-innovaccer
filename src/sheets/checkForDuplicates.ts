import { Action } from '@flatfile/configure';


export const checkForDuplicates = new Action(
  {
    slug: 'checkForDuplicates',
    label: 'Check for duplicates',
    description:
      'This action will check for and remove duplicate entries based on Claim Number. The first entry for each email will be preserved.',
  },
  async (event) => {
    const { workbookId, sheetId } = event.context

    const records = await event.api.getRecords({
      workbookId,
      sheetId,
    })

    if (!records.data?.records) return

    const uniqueClaims: string[] = []
    const recordIdsToDelete = []

    for (let [_index, record] of records.data.records.entries()) {
      const claim = record.values.claim_number.value?.toString()
      if (claim && uniqueClaims.includes(claim)) {
        recordIdsToDelete.push(record.id)
      } else if (claim) {
        uniqueClaims.push(claim)
      }
    }

    await event.api.deleteRecords({
      workbookId,
      sheetId,
      ids: recordIdsToDelete,
    })
  }
)