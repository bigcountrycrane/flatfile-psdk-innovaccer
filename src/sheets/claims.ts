import {
    BooleanField,
    DateField,
    NumberField,
    OptionField,
    Sheet,
    SpaceConfig,
    TextField,
    Workbook,
    LinkedField,
    ReferenceField
  } from '@flatfile/configure'

// import { checkForDuplicates } from './checkForDuplicates'

export const claims = new Sheet('Claims', {
    patient_name: TextField({
      label: 'Patient Name',
      description: 'This should be the patient full name.',
      required: true
    }),
    mrn: TextField({
      label: 'Medical Record Number',
      description: 'This should be an alphanumeric MRN without any special characters.',
      required: true
    }),
    claim_number: NumberField({
      label: 'Payer Name',
      required: true
    }),
    provider_npi: NumberField({
      label: 'NPI of the provider',
      required: true
    }),
    date_of_service: DateField({
      label: 'Date of Service',
    })
  },
  // {
  //   actions: {
  //     checkForDuplicates
  //     }
  // }
)