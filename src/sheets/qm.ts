import {
    BooleanField,
    DateField,
    NumberField,
    OptionField,
    Sheet,
    SpaceConfig,
    TextField,
    Workbook,
    LinkedField,
    ReferenceField
  } from '@flatfile/configure'

export const qm = new Sheet('Quality Measures', {
  vbc: ReferenceField({
    label: 'Value Based Care Contract',
    sheetKey: 'vbc',
    foreignKey: 'id'
  }),
  name: TextField({
    label: 'Quality Measure Name',
    required: true,
    unique: true,
  }),
  type: OptionField({
    label: 'Measure Type',
    required: true,
    options: {
      metric: 'Metric',
      imperial: 'Imperial',
    },
  }),
  year: DateField({
    label: 'Contract Year',
    required: true
  }),
  status: OptionField({
    label: 'Status',
    required: true,
    options: {
      active: 'Active',
      closed: 'Closed',
      future: 'Future',
    },
  })
},


// record variables and functions

// get rowId(): string | number;
// get originalValue(): M;
// get value(): M;
// private verifyField;
// private isLinkedField;
// set(field: string, value: TPrimitive): this | undefined;
// setLinkedValue(linkedFieldKey: string, childKey: string, value: TPrimitive): this;
// get(field: string): null | TPrimitive;
// getLinks(field: string): any;
// getLinkedValue(linkedFieldKey: string, childKey: string): string | number | boolean | {
//     value: TPrimitive;
//     links: TRecordData<TPrimitive>[];
// } | null;
// addInfo(fields: string | string[], message: string): this;
// addComment(fields: string | string[], message: string): this;
// addError(fields: string | string[], message: string): this;
// addWarning(fields: string | string[], message: string): this;
// pushInfoMessage(fields: string | string[], message: string, level: IRecordInfo['level'], stage: TRecordStageLevel): this;
// toJSON(): IRawRecordWithInfo<M>;

// variables available in session

// get workspaceId(): string;
// get workbookId(): string;
// get schemaId(): number;
// get schemaSlug(): string;
// get uploads(): string[];
// get endUser(): any;
// get rows(): IRawRecord[];
// get env(): Record<string, string | number | boolean> | undefined;
// get envSignature(): string | undefined;

{
  recordCompute: (record, session) => {
    console.log(session.workbookId);
    console.log(session.schemaSlug);
    console.log(session.env);
  },
}


)